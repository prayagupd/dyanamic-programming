/**
 * A.
 * 1) Write a scala program to implement a recursive version of the function to compute an
    element of the Fibonacci sequence which is defined as follows:
    Fib(0) = 0
    Fib(1) = 1
    Fib(n) = Fib(n-1) + Fib(n-2)

    2) Implement a brute force recursive version and a memoized version.
    In the memoized version, you can store the elements of the sequence in any data structure with efficient
    access capability. Include a counter of the basic operations (recursive calls).

    3) How many recursive calls are made by the non-memoized brute force version for computing
    Fib(30)?

    4) Compare the two versions (based on the resulting counts). Briefly explain why the brute
    force algorithm needs to use some form of dynamic programming?
 *
 * OUTPUT
 * ================================================
 * Number of tests : 1
  Input            : 30
  30th fib = 832040 in 0 secs/ rec calls = 2692537
  30th fib = 832040 in 0 secs/ rec calls = 31
 *
 * Created by 984493 on 6/9/2015.
 */

import scala.math._
import scala.collection.mutable.ArrayBuffer

object Fibonacci {


  var cache = scala.collection.immutable.Map[Int, BigInt]()

  var numberOfRecursiveCalls : Int = 0
  var numberOfRecursiveMemoizedCalls : Int = 0

  def recursiveBruteForceFibonacci(num : Int) : BigInt = {
    numberOfRecursiveCalls = numberOfRecursiveCalls + 1
    if (num == 0) {
      return 0
    }

    if (num == 1) {
      return 1
    }

    val fib1 = recursiveBruteForceFibonacci(num - 1)
    val fib2 = recursiveBruteForceFibonacci(num - 2)
    fib1 + fib2

  }

  def recursiveMemoizedFibonacci(num : Int) : BigInt = {
    numberOfRecursiveMemoizedCalls = numberOfRecursiveMemoizedCalls + 1

    if (num == 0) {
      return 0
    }

    if (num == 1) {
      return 1
    }

    var fib1 : BigInt = 0
    var fib2 : BigInt = 0

    if(cache.contains(num-1)) {
      val n1 = num-1
      fib1 = cache.get(num -1).get
      //println(s"#1 cache for fib(${num}) = ${fib1}")
    } else {
      fib1 = recursiveMemoizedFibonacci(num - 1)
      cache += num-1 -> fib1
    }

    if(cache.contains(num-2)) {
      val n2 = num -2
      fib2 = cache.get(num -2).get
      //println(s"#2 for ${num} from cache,  fib(${n2}) = ${fib2}")
    } else {
      fib2 = recursiveMemoizedFibonacci(num - 2)
      cache += num-2 -> fib2
    }

    cache += num -> (fib1 + fib2)
    //println(s"fib=${fib1}+${fib2}")
    fib1 + fib2

  }


  def main(args: Array[String]) {

    print("Number of tests : ")
    val tests = readInt()
    var inputBuffer = ArrayBuffer[Int]()
    for(counter <- 1 to tests) {
      print("Input : ")
      inputBuffer += readInt()
    }

    inputBuffer.foreach { input =>
      val startTime : Long = System.currentTimeMillis()
      val fib = recursiveBruteForceFibonacci(input)
      val endTime : Long = System.currentTimeMillis()
      val time = (endTime-startTime)/1000 //
      println(s"${input}th fib = ${fib} in ${time} secs/ rec calls = ${numberOfRecursiveCalls}")

      val fib2 = recursiveMemoizedFibonacci(input)
      val endTime2 = System.currentTimeMillis()
      val time2 = (endTime-startTime)/1000 //
      println(s"${input}th fib = ${fib2} in ${time2} secs/ rec calls = ${numberOfRecursiveMemoizedCalls}")
    }
  }
}